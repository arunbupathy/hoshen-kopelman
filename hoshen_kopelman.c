 
/*  This is my personal implementation of the Hoshen-Kopelman cluster
 *  finding algorithm, which is an effective method of finding and 
 *  labelling clusters of occupied (or) similar valued cells in a grid.
 *  It was first described by two gentlemen Joseph Hoshen and Raoul
 *  Kopelman in their paper on percolation [PRB 14, 3438 (1976)].
 *  Unlike the usual methods found online, this does not use the union
 *  and find methods. This works for both 2d and 3d regular grids.
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# include <stdlib.h>
# include <stdbool.h>

int find_clusters(const int * gsize, const int * occup, const bool pbc, int * label)
{
    int Lx = gsize[0];
    int Ly = gsize[1];
    int Lz = gsize[2];
    int Cells = Lx * Ly * Lz;
    
    int * pptr = (int *) malloc(sizeof(int) * Cells); // a pointer to grid cells
    
    for(int i1 = 0; i1 < Cells; ++i1)
    {
        pptr[i1] = i1; // initially make it point to itself
        label[i1] = -1; // the particle's cluster label; -1 means not labeled
    }
    
    /* scan through the grid cells to identify clusters */
    for(int i = 0; i < Lz; ++i) // loop over z
    {
        int ip = i + 1; // p stands for +1
        if(pbc)
            ip = ip % Lz; // if pbc==true, wrap around
        
        int i0 = i * Ly * Lx; // # of steps we need to move in the index to address cells with z co-ord = i
        int ip0 = ip * Ly * Lx; // same for neighbor(s) in the +z direction
        
        for(int j = 0; j < Ly; ++j) // loop over y
        {
            int jp = j + 1;
            if(pbc)
                jp = jp % Ly;
            
            int j0 = j * Lx; // # of steps we need to move in the index to address cells with y co-ord = j
            int jp0 = jp * Lx;
            
            for(int k = 0; k < Lx; ++k) // loop over x
            {
                int kp = k + 1;
                if(pbc)
                    kp = kp % Lx;
                
                int k0 = k; // this is just to keep the variable names consistent
                int kp0 = kp;
                
                int cell = i0 + j0 + k0; // this is index of the cell (i,j,k)
                
                if(occup[cell] != 0) // i.e., we take zero to be an empty cell
                {
                    
                    int nei = i0 + j0 + kp0; // this is the index of the right neighbor of cell (i,j,k)
                    // the first condition makes sure we don't look beyond the grid when pbc==false
                    if(kp < Lx && occup[nei] == occup[cell])
                    {
                        int a = cell; // take the cell (i,j,k)
                        while(pptr[a] != a)
                            a = pptr[a]; // and find the cell it is ultimately pointing to
                            
                        int b = nei; // do the same for its neighbor
                        while(pptr[b] != b)
                            b = pptr[b];
                        
                         // make the larger numbered cell point to the smaller
                        if(a > b)
                            pptr[a] = b;
                        else
                            pptr[b] = a;
                    }
                    
                    nei = i0 + jp0 + k0; // do the same for the front neighbor
                    if(jp < Ly && occup[nei] == occup[cell])
                    {
                        int a = cell;
                        while(pptr[a] != a)
                            a = pptr[a];
                            
                        int b = nei;
                        while(pptr[b] != b)
                            b = pptr[b];
                        
                        if(a > b)
                            pptr[a] = b;
                        else
                            pptr[b] = a;
                    }
                    
                    nei = ip0 + j0 + k0; // do the same for the top neighbor
                    if(ip < Lz && occup[nei] == occup[cell])
                    {
                        int a = cell;
                        while(pptr[a] != a)
                            a = pptr[a];
                            
                        int b = nei;
                        while(pptr[b] != b)
                            b = pptr[b];
                        
                        if(a > b)
                            pptr[a] = b;
                        else
                            pptr[b] = a;
                    }
                    
                }
            }
        }
    }
    
    // revisit the entire grid and give labels to the cells
    // based on which cell they ultimately point to
    int tot = 0;
    for(int i = 0; i < Lz; ++i)
    {
        int i0 = i * Ly * Lx;
        for(int j = 0; j < Ly; ++j)
        {
            int j0 = j * Lx;
            for(int k = 0; k < Lx; ++k)
            {
                int k0 = k;
                int cell = i0 + j0 + k0;
                
                if(occup[cell] != 0)
                {
                    int a = cell; // take cell (i,j,k)
                    while(pptr[a] != a)
                        a = pptr[a]; // and find the cell it ultimately points to (the root)
                    
                    if(label[a] == -1) // see if the root is not already labeled
                    {
                        label[a] = tot; // if not, give a unique label to the root
                        label[cell] = tot++; // give the same label to (i,j,k) too
                    }
                    else
                        label[cell] = label[a]; // if already labeled, copy the root's label to (i,j,k)
                }
            }
        }
    }
    
    free(pptr);
    return(tot);
}


// find clusters using the Hoshen-Kopelman algorithm

# include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

int find_clusters(const int *, const int *, const bool, int *);

#ifdef __cplusplus
}
#endif

# Hoshen-Kopelman Cluster Finding Algorithm

The Hoshen-Kopelman algorithm is an efficient method to find and label contiguous regions of occupied (or similarly labeled) cells in a d-dimensional grid. It was first desribed by Joseph Hoshen and Raoul Kopelman in their paper: [Phys. Rev. B **14**, 3438 (1976)](http://link.aps.org/doi/10.1103/PhysRevB.14.3438). This is a naive 'C' implementation of the same that is not the most optimized (but not bad either). Unlike the implementations that are commonly found on the internet, this does not make use of the union-find methods. This implementation works for both 2d and 3d (regular) grids. An example code showing how to use this function is also provided along with a makefile for easy compilation.

## Function definition:

`int find_clusters(const int gsize[3], const int * occup, const bool pbc, int * label)`

### Function arguments:

**gsize** should define the size of the grid (i.e., the number of grid cells) in the x, y and z directions. For instance, to define a 2d grid with 12 by 14 cells, do `gsize[3] = {12, 14, 1}`.

**occup** is the input array, and should hold the occupancy of each grid cell. The occupancy of the grid cell at `(x,y,z)` should be stored at the array index `(x+y*gsize[0]+z*gsize[0]*gsize[1])`. Use `0` to denote an empty cell and a positive integer to represent an occupied cell.

Note that the function distinguishes clusters with different occupancies. In other words, it only considers a contiguous region that has the same occupancy value as a cluster. This design choice was made to give more flexibility to the user.

**pbc** can be used to toggle periodic boundary conditions; true == on, false == off.

**label** is the output array that gives the label of the cluster to which the given grid cell belongs. The array indexing convention is the same as `occup`. The clusters are labeled from `0` to `(tot-1)`, where `tot` is the return value of the function = the total number of clusters. `-1` implies that the cell is empty.

Both `occup` and `label` are 1d arrays and should be `(gsize[0]*gsize[1]*gsize[2])` elements long.

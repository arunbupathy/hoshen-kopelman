CXX = g++ -Wall
CC = gcc -Wall
OPFLAGS = -O2 -march=native
LDFLAGS =

example: example.o hoshen_kopelman.o
	$(CXX) $? $(LDFLAGS) -o $@

example.o: example.cpp
	$(CXX) $(OPFLAGS) $? -c

hoshen_kopelman.o: hoshen_kopelman.c
	$(CC) $(OPFLAGS) $? -c

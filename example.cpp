 
# include <iostream>
# include <cstdlib>
# include "hoshen_kopelman.h"

int main(void)
{
    /* Consider the following 10 x 10 grid.
     * The elements of the 2d grid must be
     * represented as a 1d array. The element
     * at x,y in the 2d grid should be at
     * the array location (y*lx + x), where
     * lx is the size of the grid along x.
     */
    int grid2d[] = {
        0,  1,  1,  1,  0,  0,  0,  1,  0,  1,
        1,  0,  1,  1,  0,  0,  0,  0,  0,  0,
        0,  1,  1,  0,  0,  1,  1,  0,  0,  1,
        0,  0,  0,  1,  1,  0,  0,  1,  0,  0,
        0,  1,  0,  0,  1,  1,  1,  0,  0,  0,
        0,  1,  0,  0,  1,  1,  0,  1,  1,  0,
        1,  0,  1,  0,  0,  1,  0,  1,  0,  1,
        0,  1,  0,  0,  0,  0,  0,  0,  0,  0,
        1,  0,  1,  0,  0,  0,  1,  1,  1,  1,
        0,  1,  1,  0,  1,  1,  0,  1,  0,  1
    };
    
    int gsize[] = {10, 10, 1}; // this is the size of our grid (the z dimension is 1, because ours is a 2d grid)
    
    int labels[100]; // this will serve as the output array that stores the cluster labels
    
    /* Now let us find and label the clusters in the grid assuming there are periodic boundaires */
    int num_clusters = find_clusters(gsize, grid2d, true, labels);
    
    for(int i = 0; i < 10; ++i)
    {
        for(int j = 0; j < 10; ++j)
        {
            if(labels[i * 10 + j] > -1)
                std::cout << labels[i * 10 + j] << "\t";
            else
                std::cout << "-\t";
        }
        std::cout << std::endl;
    }
    
    std::cout << "Found a total of " << num_clusters << " clusters with pbc." << std::endl;
    
    /* Now let us try the same with no periodic boundary conditions */
    num_clusters = find_clusters(gsize, grid2d, false, labels);
    
    for(int i = 0; i < 10; ++i)
    {
        for(int j = 0; j < 10; ++j)
        {
            if(labels[i * 10 + j] > -1)
                std::cout << labels[i * 10 + j] << "\t";
            else
                std::cout << "-\t";
        }
        std::cout << std::endl;
    }
    
    std::cout << "Found a total of " << num_clusters << " clusters without pbc." << std::endl;
    
    return 0;
}
